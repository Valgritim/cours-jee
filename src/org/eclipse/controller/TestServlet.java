package org.eclipse.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.model.Personne;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/mapage")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().print("Hello world!");
		//Indique le type de r�ponse
//		response.setContentType("text/html");
//		response.setCharacterEncoding("UTF-8");
//		PrintWriter out = response.getWriter();
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"utf-8\" />");
//		out.println("<title>Projet JEE</title>");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("<h1 style=\"color: blue;\">Hello world!</h1>");
//		out.println("</body>");
//		out.println("</html>");
//		String nom = request.getParameter("nom");
//		String prenom = request.getParameter("prenom");
//		PrintWriter out = response.getWriter();
//		out.println("Hello " + nom + " " + prenom);
		
		String ville = "Mandelieu";
		request.setAttribute("maVille", ville);
		
		Personne perso = new Personne();
		perso.setNom("America");
		perso.setPrenom("Captain");
		perso.setId(1);
		request.setAttribute("personne", perso);
		this.getServletContext().getRequestDispatcher("/WEB-INF/vue.jsp").forward(request,  response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
