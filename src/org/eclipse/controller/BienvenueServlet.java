package org.eclipse.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BienvenueServlet
 */
@WebServlet("/BienvenueServlet")
public class BienvenueServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static int count;
	
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String message = "Bienvenue au cours de programmation";
		
		int compte = compterVisites();
		
		request.setAttribute("bienvenue", message);
		request.setAttribute("compte", compte);		
		
		List<String> sports = new ArrayList<String>();
		sports.add("football");
		sports.add("tennis");
		sports.add("rugby");
		sports.add("basketball");
		
		request.setAttribute("sports", sports);
		this.getServletContext().getRequestDispatcher("/WEB-INF/bienvenue.jsp").forward(request,  response);
	}
	public int compterVisites() {
		
		return count++;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	

}
