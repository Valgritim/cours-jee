package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.model.Personne;
import org.eclipse.service.PersonneDaoImpl;

/**
 * Servlet implementation class AjoutPersonneServlet
 */
@WebServlet("/AjoutPersonneServlet")
public class AjoutPersonneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutPersonne.jsp").forward(request,  response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		Personne personne = new Personne(nom,prenom);
		PersonneDaoImpl daop = new PersonneDaoImpl();
		Personne insertedPersonne = daop.save(personne);
		request.setAttribute("personne", insertedPersonne);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/confirmation.jsp").forward(request, response);			
	
	}

}
