package org.eclipse.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.eclipse.model.Personne;


public class PersonneDaoImpl implements Dao<Personne>{

	@Override
	public Personne save(Personne personne) {
		Connection connexion = MyConnection.getConnection();
		
		if(connexion != null) {		
			
				try {
					
					PreparedStatement ps = connexion.prepareStatement("INSERT INTO personne (nom,prenom) VALUES (?,?);");
					ps.setString(1, ((Personne) personne).getNom());
					ps.setString(2, ((Personne) personne).getPrenom());
					ps.executeUpdate();	
					System.out.println("La personne a �t� cr��e!");
					return personne;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		return null;

	}

}
