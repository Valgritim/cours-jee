<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
<title>Confirmation</title>
</head>
<body>
<div class="container">
	<div class="alert alert-success mt-5" role="alert">
	<p>La personne ${personne.prenom} ${personne.nom} a bien été ajoutée à la base!</p>
	</div>
</div>
</body>
</html>