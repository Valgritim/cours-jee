<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700">
<title>Formulaire d'ajout</title>
</head>
<body>
	<div class="container">
	<h1>Formulaire d'ajout de personne</h1>
		<form method="post" action="AjoutPersonneServlet">
		  <div class="form-group">
		    <label for="prenom" >Prénom</label>		    
		      <input type="text"  class="form-control" id="prenom" name="prenom" value="" placeholder="votre prénom">		   
		  </div>
		  <div class="form-group">
		    <label for="nom" >Nom</label>		    
		      <input type="text" class="form-control" name="nom" id="nom" placeholder="votre nom">		    
		  </div>
		  <input type="submit" class="btn btn-primary" value="Ajouter">
		</form>
	</div>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>