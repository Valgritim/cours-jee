<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "org.eclipse.model.*" %>
    <%@ page import = "java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Mon premier MVC en Java EE</title>
</head>
<body>
	<h2>Hello world from JSP!</h2>
	<%-- <%
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		out.println("<br/>Hello" + " " + prenom + " " + nom);
	%> --%>
<%-- 	<% 
		String notreVille = (String) request.getAttribute("maVille");		
		Personne p = (Personne) request.getAttribute("personne");
		out.println("Bienvenue à " + notreVille + " " + p.getPrenom() + " " + p.getNom());
		
	%> --%>
<%-- 	<jsp:useBean id="perso" scope="page" class="org.eclipse.entities.Personne">
		<jsp:setProperty name="perso" property="nom" value="wick"/>
		<jsp:setProperty name="perso" property="prenom" value="John"/>
	</jsp:useBean> --%>
<%-- 	<%
		out.println("Bienvenue " + perso.getNom() +" " + perso.getPrenom()+ "<br/>");
	%>
	<jsp:useBean id="personna" scope="page" class="org.eclipse.entities.Personne">
	</jsp:useBean>
	<%
		personna.setNom("Oui");
		personna.setPrenom("Oui");
		out.println("Bienvenue " + personna.getNom() +" " + personna.getPrenom());
	%> --%>
	<p>${personne.prenom} &nbsp; ${personne.nom}</p>
	
	<% 
		Date dateJour = new Date();
		for(int i = 0; i<5; i++){
			out.println("J'aime les Merguez!");
			out.println(dateJour + "<br/>");
		}
	%>
	
	<%
		List<String> saisons = new ArrayList<String>();
		saisons.add("Printemps");
		saisons.add("Eté");
		saisons.add("Automne");
		saisons.add("Hiver");
		
		for(String saison: saisons)
			out.println(saison + "<br/>");
	%>
	
</body>
</html>